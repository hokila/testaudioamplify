//
//  ViewController.h
//  testAudioAmplify
//
//  Created by Hokila Jan on 12/11/29.
//  Copyright (c) 2012年 Hokila Jan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
@interface ViewController : UIViewController<AVAudioPlayerDelegate>{
    
    NSMutableDictionary *soundDictionary;
    NSMutableArray* soundArray;
}
- (IBAction)start:(id)sender;
- (IBAction)playAndAnalyseAudio:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *analyseText;
@property (weak, nonatomic) IBOutlet UIView *averageBox;
@property (weak, nonatomic) IBOutlet UIView *peakBox;

@end
