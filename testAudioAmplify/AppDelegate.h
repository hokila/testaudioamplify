//
//  AppDelegate.h
//  testAudioAmplify
//
//  Created by Hokila Jan on 12/11/29.
//  Copyright (c) 2012年 Hokila Jan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
