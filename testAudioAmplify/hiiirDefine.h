#define ViewX(view) view.frame.origin.x
#define ViewY(view) view.frame.origin.y
#define ViewWidth(view) view.bounds.size.width
#define ViewHeight(view) view.bounds.size.height

//判斷device是否支援retina
#define DEVICE_SUPPORT_RETINA [[UIScreen mainScreen] respondsToSelector:\
@selector(displayLinkWithTarget:selector:)]\
&&([UIScreen mainScreen].scale == 2.0)

#define ShowLoading [MBProgressHUD show];
#define ShowLoadingOnSomeView(someView) [MBProgressHUD showHUDAddedTo:someView animated:YES];
#define HideLoadingOnSomeView(someView) [MBProgressHUD hideHUDForView:someView animated:YES];
#define HideLoading [MBProgressHUD dismiss];

//caches 目錄
#define DocumentsDir [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define ImageDir [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/image"]
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)(((rgbValue) & 0xFF0000) >> 16))/255.0 \
green:((float)(((rgbValue) & 0xFF00) >> 8))/255.0 \
blue:((float)((rgbValue) & 0xFF))/255.0 alpha:1.0]

/**
 * level = 0   is show all message
 * level = 3   is Normal NSLog
 */
#define DEBUG_TRACE_LEVEL 1

#ifdef DEBUG
#if (DEBUG_TRACE_LEVEL == 0)
#define HiiirLog(log,...) NSLog((@"[File %s] [Function %s] [Line %d]\n" log), __FILE__,__PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#elif (DEBUG_TRACE_LEVEL == 1)
#define HiiirLog(log,...) NSLog((@"\n[Function %s] [Line %d]\n" log), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#elif (DEBUG_TRACE_LEVEL == 2)
#define HiiirLog(log,...) NSLog((@"%s\n" log), __PRETTY_FUNCTION__, ##__VA_ARGS__)
#else
#define HiiirLog(log,...)    NSLog(log, ##__VA_ARGS__)
#endif
#else
#define HiiirLog(...) //nothing output for distribution
#endif

#ifdef DEBUG
#define echo(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#define echo(...)
#endif

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define ShowAlert(msg) { UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"確定",nil) otherButtonTitles:nil];[av show]; [av release];}
#define ShowAlertWithTitle(title,msg) { UIAlertView *av = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"確定",nil) otherButtonTitles:nil];[av show]; [av release];}
#define ShowMyAlert(msg,btnCancelTitle,btnOKTitle) { UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:btnCancelTitle otherButtonTitles:btnOKTitle, nil];[av show]; [av release];}
#define ShowMyAlertWithTag(title,msg,tag,btnCancelTitle,btnOKTitle) { UIAlertView *av = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:btnCancelTitle otherButtonTitles:btnOKTitle, nil]; [av setTag:tag]; [av show]; [av release];}
