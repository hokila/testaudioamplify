//
//  ViewController.m
//  testAudioAmplify
//
//  Created by Hokila Jan on 12/11/29.
//  Copyright (c) 2012年 Hokila Jan. All rights reserved.
//

#import "ViewController.h"
#import "hiiirDefine.h"

#define BoxWeight 10


@interface ViewController (){
    AVAudioRecorder *Recorder;
    NSTimer *Timer;
    NSInteger defaultHeight;
    
    CGFloat timeCount ,lastPower;
    
}
@property (nonatomic,retain) AVAudioPlayer *audioPlayer;
@end

@implementation ViewController
@synthesize audioPlayer;

NSString* const soundFileName = @"sound1";
NSString* const soundPlist = @"sound.plist";
CGFloat gap = 0.3;
CGFloat frequency = 0.03;

- (void)viewDidLoad
{
    [super viewDidLoad];
    defaultHeight = self.averageBox.frame.size.height;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[soundFileName stringByDeletingPathExtension] ofType:@"mp3"];
    
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    audioPlayer.delegate = self;
    audioPlayer.volume = 1.0;
    audioPlayer.numberOfLoops = 0;
    
    
    
    NSURL *url=[NSURL fileURLWithPath:@"/dev/null"];
    //以下宣告的set可以參照Apple的Settings文件
    NSDictionary *set = [NSDictionary dictionaryWithObjectsAndKeys:
                         [NSNumber numberWithFloat:44100.0],
                         AVSampleRateKey,
                         [NSNumber numberWithInt:kAudioFormatAppleLossless],
                         AVFormatIDKey,
                         [NSNumber numberWithInt:1],
                         AVNumberOfChannelsKey,
                         [NSNumber numberWithInt:AVAudioQualityMax],
                         AVEncoderAudioQualityKey,
                         nil];
    
    NSError *error;
    Recorder=[[AVAudioRecorder alloc] initWithURL:url settings:set error:&error];
    
//    NSString *plistPath = [[NSBundle mainBundle]pathForResource:[soundPlist stringByDeletingPathExtension] ofType:@"plist"];
//	NSMutableDictionary *soundListDic = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
//    soundDictionary = [soundListDic objectForKey:soundFileName];
    soundDictionary = [NSMutableDictionary dictionary];
    if (soundArray == nil) {
        soundArray = [NSMutableArray array];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)start:(id)sender {
    if (Recorder.isRecording) {
        [Recorder pause];
        [Timer invalidate];
    }
    else {
        [Recorder prepareToRecord];
        Recorder.meteringEnabled=YES;
        [Recorder record];
        
        Timer=[NSTimer scheduledTimerWithTimeInterval:0.1
                                               target:self
                                             selector:@selector(TimerCallback:)
                                             userInfo:nil repeats:YES];
    }
    
}

- (IBAction)playAndAnalyseAudio:(id)sender {
    
    if (audioPlayer.playing == YES) {
		[audioPlayer pause];
        [Timer invalidate];
    } else {
        Timer=[NSTimer scheduledTimerWithTimeInterval:frequency
                                               target:self
                                             selector:@selector(audioCallback:)
                                             userInfo:nil repeats:YES];
        
        [audioPlayer prepareToPlay];
        [self.audioPlayer setMeteringEnabled: YES];
        [audioPlayer play];
        
//        Parameter initial
        timeCount = 0;
        lastPower = 0;
    }
}

-(void)TimerCallback :(NSTimer *)timer {
    if (Recorder.isRecording) {
        [Recorder updateMeters];
        
        float avPower = [Recorder averagePowerForChannel:0];
        float peakPower = [Recorder peakPowerForChannel:0];
        [self adjustVisualBox:avPower peakPower:peakPower];

    }
    
}



-(void)audioCallback:(NSTimer*)timer{
    if (audioPlayer.playing == YES) {
        [audioPlayer updateMeters];
        
        float avPower = [audioPlayer averagePowerForChannel:0];
        float peakPower = [audioPlayer peakPowerForChannel:0];
        
        timeCount += timer.timeInterval;
        [self adjustVisualBox:avPower peakPower:peakPower];
    }
    
}

- (void)adjustVisualBox:(float)avPower peakPower:(float)peakPower {
    float avScale = ABS(1+ ((-BoxWeight-avPower)/-BoxWeight)),
    peakScale = ABS(1+ ((-BoxWeight-peakPower)/-BoxWeight));
    
    self.averageBox.frame = CGRectMake(ViewX(self.averageBox), ViewY(self.averageBox), ViewWidth(self.averageBox), defaultHeight*avScale);
    
    self.peakBox.frame = CGRectMake(ViewX(self.peakBox), ViewY(self.peakBox), ViewWidth(self.peakBox), defaultHeight*peakScale);
    
    avPower = ABS(avPower);
    
//    NSString *str = [NSString stringWithFormat:@"平均：%.01f 波峰：%.03f  time = %.02f", avPower,peakPower,timeCount];
//    NSLog(@"%@",str);
    
    if (avPower-lastPower > gap) {
        [soundArray addObject:[NSString stringWithFormat:@"%.02f",timeCount]];
        HiiirLog(@"add %.02f at %.02f",avPower,timeCount);
    }
    lastPower = avPower;
}


-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (flag) {
        HiiirLog(@"successfully !");
    }
    else
        HiiirLog(@"Fail !");
    
    [Timer invalidate];
    HiiirLog(@"soundArray count = %d",[soundArray count]);
    
    
    /*
     
     NSString *plistPath = [[NSBundle mainBundle]pathForResource:[soundPlist stringByDeletingPathExtension] ofType:@"plist"];
     NSMutableDictionary *soundListDic = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
     soundDictionary = [soundListDic objectForKey:soundFileName];
     if (soundArray == nil) {
     soundArray = [[NSMutableArray alloc]initWithArray:[soundDictionary objectForKey:@"data"]];
     }

     */
    NSMutableDictionary *soundListDic = [NSMutableDictionary dictionary];
    [soundListDic setObject:soundArray forKey:@"data"];
    [soundListDic setObject:[NSString stringWithFormat:@"%.02f",timeCount] forKey:@"time"];
    [soundListDic setObject:[NSString stringWithFormat:@"%.02f",gap] forKey:@"gap"];
    [soundListDic setObject:[NSString stringWithFormat:@"%f",(int)30/frequency] forKey:@"frequency"];

    [soundDictionary setObject:soundListDic forKey:soundFileName];
    
    NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    [soundDictionary writeToFile :[NSString stringWithFormat:@"%@/PathList.plist",savePath]  atomically : YES  ] ;
}

- (void)viewDidUnload {
    [self setAnalyseText:nil];
    [self setAverageBox:nil];
    [self setPeakBox:nil];
    [super viewDidUnload];
}
@end
